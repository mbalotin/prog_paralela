#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>

// #define DEBUG 0            // comentar esta linha quando for medir tempo
#define ARRAY_SIZE 1000000      // trabalho final com o valores 10.000, 100.000, 1.000.000

void bs(int n, int * vetor)
{
    int c=0, d, troca, trocou =1;

    while (c < (n-1) & trocou )
        {
        trocou = 0;
        for (d = 0 ; d < n - c - 1; d++)
            if (vetor[d] > vetor[d+1])
                {
                troca      = vetor[d];
                vetor[d]   = vetor[d+1];
                vetor[d+1] = troca;
                trocou = 1;
                }
        c++;
        }
}

/* recebe um ponteiro para um vetor que contem as mensagens recebidas dos filhos e            */
/* intercala estes valores em um terceiro vetor auxiliar. Devolve um ponteiro para este vetor */

int *interleaving(int vetor[], int tam)
{
	int *vetor_auxiliar;
	int i1, i2, i_aux;

	vetor_auxiliar = (int *)malloc(sizeof(int) * tam);

	i1 = 0;
	i2 = tam / 2;

	for (i_aux = 0; i_aux < tam; i_aux++) {
		if (((vetor[i1] <= vetor[i2]) && (i1 < (tam / 2)))
		    || (i2 == tam))
			vetor_auxiliar[i_aux] = vetor[i1++];
		else
			vetor_auxiliar[i_aux] = vetor[i2++];
	}

	return vetor_auxiliar;
}


int main(int argc, char **argv )
{
	int *vetor;
	int my_rank;    // Identificador deste processo
	int proc_n;     // Numero de processos disparados pelo usuário na linha de comando (np)
	int i;
	int tamanho;	// Tamanho do vetor a ser trabalhado.
	int parentProcess; // processo pai
    	MPI_Status status; // Status de retorno
    	double t1,t2;

    	MPI_Init(&argc,&argv);     // funcao que inicializa o MPI, todo o código paralelo esta abaixo

    	MPI_Comm_rank(MPI_COMM_WORLD,&my_rank);
    	MPI_Comm_size(MPI_COMM_WORLD,&proc_n);
	int DELTA =  ARRAY_SIZE/(proc_n/2 +1);
	#ifdef DEBUG
	printf("DELTA %d\n.",DELTA);
	#endif
	if (my_rank == 0){ // root
        printf("O tamanho de Delta é: %d\n.",DELTA);
		vetor = malloc(ARRAY_SIZE * sizeof(int));
		tamanho = ARRAY_SIZE;
	    	for (i=0 ; i<ARRAY_SIZE; i++) //init array with worst case for sorting
		        vetor[i] = ARRAY_SIZE-i;

        t1 = MPI_Wtime();
		#ifdef DEBUG
		for (i = 0 ; i < ARRAY_SIZE ; i++)
			printf("%d ", vetor[i]);
		printf("Root \n.");
		#endif
	}else{
	    #ifdef DEBUG
        printf("%d waiting job\n",my_rank);
		#endif
		MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
	 	MPI_Get_count(&status, MPI_INT, &tamanho);
		vetor = malloc(sizeof(int) * tamanho);
	    #ifdef DEBUG
        printf("%d alocated array of %d \n",my_rank, tamanho);
		#endif
		MPI_Recv(vetor, tamanho, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
		parentProcess = status.MPI_SOURCE;
	    #ifdef DEBUG
        printf("%d received task from %d\n",my_rank, parentProcess);
		#endif
	}
	#ifdef DEBUG
	printf("myrank  %d my size %d\n.",my_rank, tamanho);
	printf("%d reaches ----Phase 2 \n.", my_rank);
	#endif
	if (tamanho <= DELTA  ){
		#ifdef DEBUG
		printf("process %d sorting.", my_rank);
		#endif
		bs(tamanho, vetor);
	} else {
		#ifdef DEBUG
		printf("ELSE :D\n");
		#endif
		int rankLeft = my_rank*2+1;
		int rankRight = my_rank*2+2;
		#ifdef DEBUG
		printf("Process %d sending to  %d and %d .\n", my_rank, rankLeft, rankRight);
		#endif
		MPI_Send(&vetor[0], tamanho/2, MPI_INT, rankLeft, 0 , MPI_COMM_WORLD);
		MPI_Send(&vetor[tamanho/2], tamanho - tamanho/2, MPI_INT, rankRight, 0 , MPI_COMM_WORLD);

		#ifdef DEBUG
		printf("Process %d receiving from %d and %d .\n", my_rank, rankLeft, rankRight);
		#endif
		MPI_Recv(vetor, tamanho/2, MPI_INT, rankLeft, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
		MPI_Recv(&vetor[tamanho/2], tamanho - tamanho/2, MPI_INT, rankRight, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

		#ifdef DEBUG
		printf("\nReceived.\n");
		#endif
		vetor = interleaving(vetor, tamanho);

	}
	if (my_rank == 0){
		#ifdef DEBUG
		for (i = 0 ; i < ARRAY_SIZE ; i++)
			printf("%d ", vetor[i]);
		#endif
        t2 = MPI_Wtime();
        printf("SORTED! Total execution time %f\n", t2 - t1);
	}else
		MPI_Send(vetor, tamanho, MPI_INT, parentProcess, 0 , MPI_COMM_WORLD);

	MPI_Finalize();
	return 0;
}
