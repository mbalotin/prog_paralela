#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv){
    int my_rank;    // Identificador deste processo
    int proc_n;     // Numero de processos disparados pelo usuário na linha de comando (np)
    int message[10];    // Buffer para as mensagens
    double t1,t2;
    int i ;
    MPI_Status status; // Status de retorno

    MPI_Init(&argc,&argv);     // funcao que inicializa o MPI, todo o código paralelo esta abaixo

    MPI_Comm_rank(MPI_COMM_WORLD,&my_rank);
    MPI_Comm_size(MPI_COMM_WORLD,&proc_n);
    if ( my_rank == 0 ){ //start the message array
        t1 = MPI_Wtime();
        int i ;
        printf("proc_n %d ", proc_n);
        printf("original message:");
        for ( i = 0 ; i < 10 ; i++){
            message[i] = i;
            printf("%d,", message[i]);
        }
        printf("\n");
    }

    for (i = 0; i < 10; i++){
        if ( my_rank != 0 )
            MPI_Recv (&message[i], sizeof(int), MPI_INT, my_rank-1, 50, MPI_COMM_WORLD, &status);
        if ( my_rank == proc_n - 1 )
            printf("Mensagem: %d\n", message[i]);
        else{
            t1 = MPI_Wtime();
            MPI_Send (&message[i], sizeof(int), MPI_INT, my_rank + 1, 50, MPI_COMM_WORLD);
            t2 = MPI_Wtime();
            printf("Send from %d to %d, message[%d]: %d in %f\n", my_rank, my_rank+1, i, message[i], (t2-t1)/2 );
        }
    }

    if ( my_rank == proc_n - 1 )
        MPI_Send (0, sizeof(int), MPI_INT, 0, 50, MPI_COMM_WORLD);
    else if (my_rank == 0){
        MPI_Recv (&i, sizeof(int), MPI_INT, proc_n - 1, 50, MPI_COMM_WORLD, &status);
        t2 = MPI_Wtime();
        printf("Total execution time %f", t2 - t1);
    }

    MPI_Finalize();
}
