#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>

#define ARRAY_SIZE 100000
#define PERC_ALLOC_LOCAL 0

void bs(int n, int * vetor){
    int c=0, d, troca, trocou =1;

    while (c < (n-1) & trocou ){
        trocou = 0;
        for (d = 0 ; d < n - c - 1; d++)
            if (vetor[d] > vetor[d+1]){
                troca      = vetor[d];
                vetor[d]   = vetor[d+1];
                vetor[d+1] = troca;
                trocou = 1;
            }
        c++;
    }
}

/* recebe um ponteiro para um vetor que contem as mensagens recebidas dos filhos e            */
/* intercala estes valores em um terceiro vetor auxiliar. Devolve um ponteiro para este vetor */

int *interleaving(int vetor[], int tam){
    int *vetor_auxiliar;
    int i1, i2, i_aux;
    vetor_auxiliar = (int *)malloc(sizeof(int) * tam);
    i1 = 0;
    i2 = tam / 2;
    for (i_aux = 0; i_aux < tam; i_aux++) {
        if (((vetor[i1] <= vetor[i2]) && (i1 < (tam / 2)))|| (i2 == tam))
            vetor_auxiliar[i_aux] = vetor[i1++];
        else
            vetor_auxiliar[i_aux] = vetor[i2++];
    }
    return vetor_auxiliar;
}


int main(int argc, char **argv ){
    int *vetor;
    int my_rank;    // Identificador deste processo
    int proc_n;     // Numero de processos disparados pelo usuário na linha de comando (np)
    int i;
    int tamanho;	// Tamanho do vetor a ser trabalhado.
    int offset;     // posição inicial do vetor a ser enviado para os filhos.
    int parentProcess; // processo pai
    MPI_Status status; // Status de retorno
    double t1,t2;
    
    MPI_Init(&argc,&argv);     // funcao que inicializa o MPI, todo o código paralelo esta abaixo
    
    MPI_Comm_rank(MPI_COMM_WORLD,&my_rank);
    MPI_Comm_size(MPI_COMM_WORLD,&proc_n);
    int DELTA =  ARRAY_SIZE/(proc_n/2 +1);
    if (my_rank == 0){ // root
        printf("O tamanho de Delta é: %d\n.",DELTA);
        vetor = malloc(ARRAY_SIZE * sizeof(int));
        tamanho = ARRAY_SIZE;
            for (i=0 ; i<ARRAY_SIZE; i++) //init array with worst case for sorting
                vetor[i] = ARRAY_SIZE-i;
        t1 = MPI_Wtime();
    }else{
        MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        MPI_Get_count(&status, MPI_INT, &tamanho);
        vetor = malloc(sizeof(int) * tamanho);
        MPI_Recv(vetor, tamanho, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        parentProcess = status.MPI_SOURCE;
    }
    if (tamanho <= DELTA  ){
        bs(tamanho, vetor);
    } else {
        int rankLeft = my_rank*2+1;
        int rankRight = my_rank*2+2;
        offset = tamanho*PERC_ALLOC_LOCAL/100;
        int tamanhoLeft = (tamanho - offset)/2;
        int tamanhoRight = (tamanho - offset) - tamanhoLeft;
        int startLeft = offset;
        int startRight = tamanhoLeft;

        MPI_Send(&vetor[startLeft], tamanhoLeft, MPI_INT, rankLeft, 0 , MPI_COMM_WORLD);
        MPI_Send(&vetor[startRight], tamanhoRight, MPI_INT, rankRight, 0 , MPI_COMM_WORLD);
        bs(offset, vetor);
        MPI_Recv(&vetor[startLeft], tamanhoLeft, MPI_INT, rankLeft, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        MPI_Recv(&vetor[startRight], tamanhoRight, MPI_INT, rankRight, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

        vetor = interleaving(vetor, tamanho);
    }
    if (my_rank == 0){
        t2 = MPI_Wtime();
        printf("SORTED! Total execution time %f\n", t2 - t1);
    }else
    	MPI_Send(vetor, tamanho, MPI_INT, parentProcess, 0 , MPI_COMM_WORLD);
    MPI_Finalize();
    return 0;
}
