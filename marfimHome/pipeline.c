#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv){
    int my_rank;    // Identificador deste processo
    int proc_n;     // Numero de processos disparados pelo usuário na linha de comando (np)
    int message;    // Buffer para as mensagens
    MPI_Status status; /* Status de retorno */

    MPI_Init(&argc,&argv);     // funcao que inicializa o MPI, todo o código paralelo esta abaixo

    MPI_Comm_rank(MPI_COMM_WORLD,&my_rank);
    MPI_Comm_size(MPI_COMM_WORLD,&proc_n);

    // receber da esquerda
    if ( my_rank == 0 )     // sou o primeiro?
        message = 0;        // sim, sou o primeiro, crio a mensagem sem receber
    else
        MPI_Recv (&message, 100, MPI_INT, my_rank-1, 50, MPI_COMM_WORLD, &status);

    // processo mensagem
    message++;  // incremento um na mensagem recebida

    // enviar para a direita
    if ( my_rank == proc_n-1 )  // sou o último?
        printf("Mensagem: %d\n", message);  // sim sou o último, apenas mostro mensagem na tela
    else
        MPI_Send (&message, 1, MPI_INT, my_rank+1, 50, MPI_COMM_WORLD);

    MPI_Finalize();
}
