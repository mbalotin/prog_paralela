#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MATRIX_ROWS 100
#define MATRIX_COLUMNS 10

#define ROWS_PER_MESSAGE 1

#define MPI_SUICIDE_TAG MATRIX_ROWS

int **matriz;
/* Vetor que o mestre usará para gerar um vetor inverso a ser copiado para o saco de trabalho e receber os resultados.
 * E o escravo usará para receber e ordenar os vetores.
 */
int *vetorAux;
int **matrizAux;

int compareTo (const void * a, const void * b)
{
    return ( *(int*)a - *(int*)b );
}

int main(int argc, char **argv)
{
    int my_rank;    // Identificador deste processo
    int proc_n;     // Numero de processos disparados pelo usuário na linha de comando (np)
    int sortedRows = 0;

    MPI_Status statusRec, statusSnd; /* Status de retorno */
    MPI_Request requestRec, requestSnd;

    MPI_Init(&argc,&argv);     // funcao que inicializa o MPI, todo o código paralelo esta abaixo

    MPI_Comm_rank(MPI_COMM_WORLD,&my_rank);
    MPI_Comm_size(MPI_COMM_WORLD,&proc_n);

    if( my_rank == 0)
    {
        double t1,t2;
        int i,j;
        if ((matriz = malloc(MATRIX_ROWS * sizeof(int *))) == NULL || (vetorAux = malloc(MATRIX_COLUMNS * sizeof(int *))) == NULL)
        {
            printf("Malloc Failed");
            return 0;
        }
        // Popula o vetor base invertido, pior caso de quick sort.
        for (i = 0; i<MATRIX_COLUMNS; i++)
            vetorAux[i] = MATRIX_COLUMNS - i;
        // Copia o vetor base para todos as entredas da matriz, assim garantimos que todas as execuções terão o mesmo número de instruções.
        for (i = 0; i< MATRIX_ROWS; i++)
        {
            matriz[i] = malloc(MATRIX_COLUMNS * sizeof(int));
            memcpy(matriz[i], vetorAux, MATRIX_COLUMNS * sizeof(int));
        }
        // Inicia a contagem de tempo
        t1 = MPI_Wtime();
        //Envia um vetor para cada processo iniciar;
        for (i = 0 ; i < (proc_n -1) ; i++)
            MPI_Send (matriz[i], MATRIX_COLUMNS, MPI_INT, (i % (proc_n-1)) + 1 ,i, MPI_COMM_WORLD);
        // Enquanto todos os registros da tabela não forem ordenados, aguarda o recebimento de algum vetor ordenado, e passa um novo vetor para o processo.
        while (sortedRows < MATRIX_ROWS)
        {
            if (i <  MATRIX_ROWS && sortedRows > 0 )
            {
                MPI_Isend (matriz[i], MATRIX_COLUMNS * ROWS_PER_MESSAGE, MPI_INT, statusRec.MPI_SOURCE, i, MPI_COMM_WORLD, &requestSnd);
                i += ROWS_PER_MESSAGE;
                MPI_Wait(&requestRec, &statusRec);
            }
            MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &statusRec);
            MPI_Irecv (matriz[statusRec.MPI_TAG], MATRIX_COLUMNS * ROWS_PER_MESSAGE, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &requestRec);
            sortedRows++;
            if (i <  MATRIX_ROWS && sortedRows > 1 )
                MPI_Wait(&requestSnd, &statusSnd);
        }
        // Envia uma mensagem de suicidio
        for (i = 1; i < proc_n ; i++)
            MPI_Send (0,0 , MPI_INT, i, MPI_SUICIDE_TAG, MPI_COMM_WORLD);
        t2 = MPI_Wtime();
        printf("\nOrdenação completa em %f.\n\n",t2 -t1);
    }
    else     // Slave
    {
        if ((matrizAux = malloc(MATRIX_COLUMNS * sizeof(int *) * ROWS_PER_MESSAGE)) == NULL)
        {
            printf("Malloc Failed");
            return 0;
        }
        // Em loop, recebe um vetor, ordena e o envia para o mestre, até receber a mensagem de suicidio.
        while (1)
        {
            MPI_Probe(0, MPI_ANY_TAG, MPI_COMM_WORLD, &statusRec);
            if (statusRec.MPI_TAG == MPI_SUICIDE_TAG)
                break;
            MPI_Recv (matrizAux, MATRIX_COLUMNS * ROWS_PER_MESSAGE, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &statusRec);


            //FORCAR AQUI MPI ETC
            int i = 0;
            for (i = 0; i < ROWS_PER_MESSAGE ; i++)
                qsort(matrizAux[i], MATRIX_COLUMNS, sizeof(int), compareTo);

            if (sortedRows > 0)
                MPI_Wait(&requestSnd, &statusSnd);
            sortedRows++;
            MPI_Isend(matrizAux, MATRIX_COLUMNS * ROWS_PER_MESSAGE, MPI_INT, 0, statusRec.MPI_TAG, MPI_COMM_WORLD,&requestSnd);
        }
    }
    //printf("Ordenação completa em %.2f segundos.", time_spent);
    MPI_Finalize();
    return 0;
}
