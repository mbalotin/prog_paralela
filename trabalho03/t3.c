#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// #define DEBUG

#define MATRIX_ROWS 10
#define MATRIX_COLUMNS 10

#define ROWS_PER_MESSAGE 5

#define MPI_SUICIDE_TAG MATRIX_ROWS

int *matriz;
/* Vetor que o mestre usará para gerar um vetor inverso a ser copiado para o saco de trabalho e receber os resultados.
 * E o escravo usará para receber e ordenar os vetores.
 */
int *vetorAux;


int compareTo (const void * a, const void * b)
{
    return ( *(int*)a - *(int*)b );
}
int main(int argc, char **argv)
{

    int i,j;
    int my_rank;    // Identificador deste processo
    int proc_n;     // Numero de processos disparados pelo usuário na linha de comando (np)
    int sortedRows = 0;

    MPI_Status statusRec, statusSnd; /* Status de retorno */
    MPI_Request requestRec, requestSnd;

    MPI_Init(&argc,&argv);     // funcao que inicializa o MPI, todo o código paralelo esta abaixo

    MPI_Comm_rank(MPI_COMM_WORLD,&my_rank);
    MPI_Comm_size(MPI_COMM_WORLD,&proc_n);

    if( my_rank == 0)
    {
        double t1,t2;
        if ((matriz = malloc(MATRIX_ROWS * MATRIX_COLUMNS * sizeof(int *))) == NULL )
        {
            printf("Malloc Failed");
            return 0;
        }
        // Copia o vetor base para todos as entredas da matriz, assim garantimos que todas as execuções terão o mesmo número de instruções.
        for (i = 0; i< MATRIX_ROWS; i++)
        {
            for (j = 0; j<MATRIX_COLUMNS; j++)
            {
                matriz[i * MATRIX_COLUMNS + j] = MATRIX_COLUMNS - j;

#ifdef DEBUG
                printf("[%d][%d](%d)  ", i, j, matriz[i * MATRIX_COLUMNS + j]);
#endif // DEBUG
            }
            printf("\n");
        }

        // Inicia a contagem de tempo
        t1 = MPI_Wtime();

        //Envia um vetor para cada processo iniciar;
        i = 0;
        int k = 1;
        for (k = 1 ; k < proc_n ; k++ )
        {
            printf("Master send to %d\n",k);
            printf("Matrix position [%d], value %d\n",i * MATRIX_COLUMNS, matriz[i * MATRIX_COLUMNS]);
            MPI_Send (&matriz[i * MATRIX_COLUMNS] , MATRIX_COLUMNS * ROWS_PER_MESSAGE, MPI_INT, k ,i, MPI_COMM_WORLD);
            i += ROWS_PER_MESSAGE;
        }

        // Enquanto todos os registros da tabela não forem ordenados, aguarda o recebimento de algum vetor ordenado, e passa um novo vetor para o processo.
        while (sortedRows < MATRIX_ROWS)
        {
            if (i < MATRIX_ROWS && sortedRows > 0 )
            {
                MPI_Isend (&matriz[i*MATRIX_COLUMNS], MATRIX_COLUMNS * ROWS_PER_MESSAGE, MPI_INT, statusRec.MPI_SOURCE, i, MPI_COMM_WORLD, &requestSnd);
                i += ROWS_PER_MESSAGE;
                MPI_Wait(&requestRec, &statusRec);
            }
            MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &statusRec);
            //statusRec.MPI_TAG
            MPI_Irecv (&matriz[statusRec.MPI_TAG * MATRIX_COLUMNS] , MATRIX_COLUMNS * ROWS_PER_MESSAGE, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &requestRec);
            sortedRows += ROWS_PER_MESSAGE;
            if (i <  MATRIX_ROWS && sortedRows > ROWS_PER_MESSAGE )
                MPI_Wait(&requestSnd, &statusSnd);
        }

        // Envia uma mensagem de suicidio
        for (i = 1; i < proc_n ; i++)
            MPI_Send (0,0 , MPI_INT, i, MPI_SUICIDE_TAG, MPI_COMM_WORLD);

        t2 = MPI_Wtime();
        printf("\nOrdenação completa em %f.\n\n",t2 -t1);

#ifdef DEBUG
        for (i = 0 ; i < MATRIX_ROWS ; i++)
        {
            for (j = 0 ; j < MATRIX_COLUMNS; j++)
                printf("%d ,", matriz[i * MATRIX_COLUMNS + j ]);
            printf("\n");
        }
#endif // DEBUG
    }
    else     // Slave
    {
        printf("slave rank: %d passei aqui :D",my_rank);
        if ((matriz = malloc(ROWS_PER_MESSAGE * MATRIX_COLUMNS * sizeof(int *) )) == NULL)
        {
            printf("Malloc Failed");
            return 0;
        }

        // Em loop, recebe um vetor, ordena e o envia para o mestre, até receber a mensagem de suicidio.
        while (1)
        {
            MPI_Probe(0, MPI_ANY_TAG, MPI_COMM_WORLD, &statusRec);
            if (statusRec.MPI_TAG == MPI_SUICIDE_TAG)
                break;
            MPI_Recv (matriz, MATRIX_COLUMNS * ROWS_PER_MESSAGE, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &statusRec);


            //FORCAR AQUI MPI ETC
#ifdef DEBUG
            printf("slave rank: %d passei aqui",my_rank);
#endif

            int i = 0;
            for (i = 0; i < ROWS_PER_MESSAGE ; i++)
                qsort(&matriz[i * MATRIX_COLUMNS], MATRIX_COLUMNS, sizeof(int), compareTo);

            if (sortedRows > 0)
                MPI_Wait(&requestSnd, &statusSnd);
            sortedRows += ROWS_PER_MESSAGE;
            MPI_Isend(matriz, MATRIX_COLUMNS * ROWS_PER_MESSAGE, MPI_INT, 0, statusRec.MPI_TAG, MPI_COMM_WORLD,&requestSnd);
        }
    }
    //printf("Ordenação completa em %.2f segundos.", time_spent);
    MPI_Finalize();
    return 0;
}
