#include <stdio.h>
#include <stdlib.h>

#define MATRIX_ROWS 1000
#define MATRIX_COLUMNS 100000
#define MAX_ALLOC_ROWS 500
int **vetor;

int compareTo (const void * a, const void * b)
{
   return ( *(int*)a - *(int*)b );
}

int main(int argc, char **argv){
    int my_rank;    // Identificador deste processo
    int proc_n;     // Numero de processos disparados pelo usuário na linha de comando (np)
    double t1,t2;
    int i, j;
    int lastSortCount = 0;
    srand(time(NULL));
    printf("Carregando Matriz inicial.\n");

    if ((vetor = malloc(MATRIX_ROWS * sizeof(int *))) == NULL){
        printf("Malloc Failed");
        return 0;
    }
    for (i = 0; i< MATRIX_ROWS; i++){
        vetor[i] = malloc(MATRIX_COLUMNS * sizeof(int));
        for (j = 0; j<MATRIX_COLUMNS; j++)
            vetor[i][j] = rand();
        if (i % MAX_ALLOC_ROWS == 0 || i == MATRIX_ROWS - 1){
            printf("started Sorting from %d to %d rows", lastSortCount,i);
            for (j = lastSortCount ; j < i;j++){
                qsort(vetor[j], MATRIX_COLUMNS, sizeof(int), compareTo);
                free(vetor[j]);
            }
            printf("   Sorted! last sorted row was: %d\n", lastSortCount);
            lastSortCount = i;
        }
    }
    printf("Ordenação completa!.");
}
